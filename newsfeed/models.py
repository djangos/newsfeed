from django.db import models


class News(models.Model):

    class Meta:
        abstract = True

    title = models.CharField(max_length=255)
    announce = models.TextField(max_length=1024)
    image = models.ImageField()
